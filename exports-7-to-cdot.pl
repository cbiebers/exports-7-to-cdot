#!/usr/bin/perl 
# exports2cdot.pl
# - Parse the contents of a 7mode /etc/exports file and generate the commands to create a 
#   CDOT export policy.  Set export permissions at a volume level to the union of the 
#   volume and any qtree/subdirectory export permissions.
#
# - Note - This rolls up all qtree exports to volume level exports only.  Since we have to
#   union permissions to make CDOT work, we may as well not have qtree export policies. 
#
use strict;
use Getopt::Std;
use Data::Dumper;

my $version="1.4";
my $exportfile="exports";
my $translationfile="translation";
my $outputfile="cdot_create_script";
my $translate="false";
my $srccontroller="";
my %vol_trans_hash = (); # Used for old -> new name translation
my %vol_opt_hash = ();   # key = export_volume, Value = opt:opt:...:opt
my %vol_ro_hash = ();    # Key = export_volume, Value = host:host:host:...:host
my %vol_rw_hash = ();	   # Key = export_volume, Value = host:host:host:...:host
my %vol_root_hash = ();  # Key = export_volume, Value = host:host:host:...:host
my @output;              # Array of strings - contents of the CDOT script.
my (%opts,$verbose,$debug,$vol_prefix);

print "\n\nexports2cdot v. $version\n";
print "-------------------\n\n";
# Get command line options.
getopts('f:t:s:m:o:hvd', \%opts)|| die "Invalid syntax.  Use: exports2cdot -f export_file [-o output_file] [-m /mntpt] [-t translation_file -s src_controller] [-h] [-v] [-d]\n\nTRANSLATION FILE FORMAT: src_controller,src_volume,dest_svm,dest_volume\n\nTranslation file is used to rename ALL volumes to match a new CDOT naming convention.\nVolumes that do not exist in the translation file are skipped in parsing the export_file.  \n\n";
# -f filename is required
if (!$opts{f}) {usage(); exit 1};
# -h displays help and exits.
if ($opts{h}) {usage(); exit 0}
# -m mount_prefix - mount the volume to a junction-path beginning with this prefix, then the volume name
if ($opts{m}) {$vol_prefix=$opts{m};print "Junction Path Prefix: $vol_prefix specified.\n"}
# -v enables verbose output to the screen.
if ($opts{v}) {$verbose="true";print "VERBOSE enabled.\n"}
# -d enables debug output to the screen.
if ($opts{d}) {$debug="true";print "DEBUG enabled.\n"}
# If -o is used, override the default output file.
if ($opts{o}) {
  if (-e $opts{o}) {
    die "Output file already exists.\n";    
  } else {
    $outputfile = $opts{o};
  }
}
# If -t is used load the translation file.
if ($opts{t}) {
  if ($opts{s}) {
    # Translation is to be done
    $translate="true";
    # Use the name provided with -s to identify the source controller for the translation.
    $srccontroller=$opts{s};
    # If the file exists, use it, if not load the default file.
    if (-e $opts{t}) {
      print "	Loading translation file: $opts{t}.\n";
      read_voltrans_file($opts{t},$srccontroller,\%vol_trans_hash);
    } else {
      print "	$opts{t} not found.\n	Loading default translation file: $translationfile.\n";
      read_voltrans_file($translationfile,$srccontroller,\%vol_trans_hash);
    }
  } else {
    die "Exiting:  You must use -t with -s.\n"
  }  
}
# Argument -f filename is Required, if the specified file exists, load and parse it.
if ($opts{f} && -e $opts{f}){
  print "	Parsing exports file: $opts{f}\n";
  parse_exports($opts{f});
  # Dump the contents of the hashes that have been constructed.
  if ($debug) {
    print "\n\n ----------------| Hash Contents |----------------\n";
    print "\nTranslate\n";
    print Dumper(\%vol_trans_hash );
    print "\nOptions\n";
    print Dumper(\%vol_opt_hash);
    print "\nRO\n";
    print Dumper(\%vol_ro_hash);
    print "\nRW\n";
    print Dumper(\%vol_rw_hash);
    print "\nROOT\n";
    print Dumper(\%vol_root_hash);
  }
} else { die "File $opts{f} not found.\n";}

# Generate Script
generate_cdot_script($translate, $srccontroller, \%vol_trans_hash, \%vol_opt_hash, \%vol_ro_hash, \%vol_rw_hash, \%vol_root_hash, \@output); 
# Output Script
output_cdot_script($outputfile, \@output);

# Completed normally.
exit 0;


#######################################################################################
# Subroutines
#######################################################################################
sub parse_exports {
  my $infile=$_[0];
  
  if ($verbose) {print "	parse_exports($infile)\n"}; 
  open(EXFIL,$infile)||die "Failed to open: [$infile]";
  while (<EXFIL>) {
    if ($_ =~ /^\s*#/) { next; }    # Skip lines beginning with #
    if ($_ =~ /^\s*$/) { next; }    # Skip lines with only whitespace
    if ($_ =~ /-actual=/) { 
      # Handle export lines containing -actual=
      if($debug) { print ("              export contains actual=   ", $_ ,"\n"); }
      parse_line_actual($_);
    } else {
      parse_line($_); 
    }
  }
  close(EXFIL);
  if ($verbose) {print "Completed parse_exports\n"}
  return;
}


sub parse_line {
  my $line = $_[0]; 
  my ($export, $options,$temp);
  my @exp_elements = ();
  my @opt_elements = ();
  my ($vol, $qtree, $dir);
  
  # Remove trailing \n 
  chomp($line);  
  if ($verbose) {print "	  parse_line($line)\n"}; 
  #Split into exported path and export options
  ($export, $options) = split (/\s+/,$line);     
  #Remove leading /vol/
  $export =~ s/^\/vol\///;
  #Extract volume, qtree, dir from the path     
  @exp_elements = split (/\//,$export,3);
  if (exists $exp_elements[0]) {$vol = $exp_elements[0];} else {$vol ="";}
  if (exists $exp_elements[1]) {$qtree = $exp_elements[1];}  else {$qtree ="";}
  if (exists $exp_elements[2]) {$dir = $exp_elements[2];} else {$dir ="";}
  if ($debug) {print "		EE>@exp_elements<   --> VOL: $vol, QTREE: $qtree, DIR: $dir<\n"}
  # Break down the export options into the individual elements
  # Remove leading -
  $options =~ s/^-//;
  @opt_elements = split (/,/,$options);
  foreach $temp (@opt_elements) {
    if ($debug) {print "		OE >$temp<\n";}   
    #sec=sectype (sys|krb5|krb5i|krb5p)
     if ($temp =~ m/^sec=/) {
       add_element($vol, $temp, \%vol_opt_hash);
     }   
    #actual=path
    if ($temp =~ m/actual=/) { 
      add_element($vol, $temp, \%vol_opt_hash);
    }   
    #anon=uid|name
    if ($temp =~ m/anon=/) {
      add_element($vol, $temp, \%vol_opt_hash);
    }
    #nosuid
    if ($temp =~ m/^nosuid$/) { 
      add_element($vol, $temp, \%vol_opt_hash);
    }    
    #ro  
    if ($temp =~ m/^ro$/) { 
      add_element($vol, $temp, \%vol_opt_hash);
    }
    #rw
    if ($temp =~ m/^rw$/) {   
      add_element($vol, $temp, \%vol_opt_hash);
    }
    #
    # Transition from single value to : delimited value elements...
    #
    #ro=hostname[:hostname]
    if ($temp =~ m/^ro=/) { 
      #Remove ro= from the beginning of the line 
      $temp =~ s/^ro=//;   
      add_elements($vol, $temp, \%vol_ro_hash);
    }  
    #rw=hostname[:hostname]
    if ($temp =~ m/^rw=/) {
      #Remove rw= from the beginning of the line 
      $temp =~ s/^rw=//;   
      add_elements($vol, $temp, \%vol_rw_hash);
    }    
    #root=hostname[:hostname]
    if ($temp =~ m/^root=/) {
      #Remove rw= from the beginning of the line 
      $temp =~ s/^root=//;   
      add_elements($vol, $temp, \%vol_root_hash);
    }           
  #END: foreach $temp (@opt_elements)    
  }  
  return; 
#END: sub parse_line
}

sub parse_line_actual {
  my $line = $_[0];
  my ($export, $options,$temp);
  my @exp_elements = ();
  my @opt_elements = ();
  my ($vol, $qtree, $dir);

  # Remove trailing \n
  chomp($line);
  if ($verbose) {print "          parse_line_actual($line)\n"};
  #Split into exported path and export options
  ($export, $options) = split (/\s+/,$line);

  # Since we know there's an actual= option, we know the export element of this line is wrong, ignore it for now.
  
  # Break down the export options into the individual elements
  # Remove leading -
  $options =~ s/^-//;
  @opt_elements = split (/,/,$options);
  foreach $temp (@opt_elements) {
    if ($debug) {print "                OE >$temp<\n";}
    #sec=sectype (sys|krb5|krb5i|krb5p)
     if ($temp =~ m/^sec=/) {
       add_element($vol, $temp, \%vol_opt_hash);
     }
    #actual=path
    if ($temp =~ m/actual=/) {
      # Don't add this to the options, we'll just put the export
      #add_element($vol, $temp, \%vol_opt_hash);
      #
      #Remove leading actual=
      $temp =~ s/actual=//;      
      #Remove leading /vol/
      $temp =~ s/^\/vol\///;
      #Extract volume, qtree, dir from the path
      @exp_elements = split (/\//,$temp,3);
      if (exists $exp_elements[0]) {$vol = $exp_elements[0];} else {$vol ="";}
      if (exists $exp_elements[1]) {$qtree = $exp_elements[1];}  else {$qtree ="";}
      if (exists $exp_elements[2]) {$dir = $exp_elements[2];} else {$dir ="";}
      if ($debug) {print "          EE>@exp_elements<   --> VOL: $vol, QTREE: $qtree, DIR: $dir<\n"}
    }
    #anon=uid|name
    if ($temp =~ m/anon=/) {
      add_element($vol, $temp, \%vol_opt_hash);
    }
    #nosuid
    if ($temp =~ m/^nosuid$/) {
      add_element($vol, $temp, \%vol_opt_hash);
    }
    #ro
    if ($temp =~ m/^ro$/) {
      add_element($vol, $temp, \%vol_opt_hash);
    }
    #rw
    if ($temp =~ m/^rw$/) {
      add_element($vol, $temp, \%vol_opt_hash);
    }
    #
    # Transition from single value to : delimited value elements...
    #
    #ro=hostname[:hostname]
    if ($temp =~ m/^ro=/) {
      #Remove ro= from the beginning of the line
      $temp =~ s/^ro=//;
      add_elements($vol, $temp, \%vol_ro_hash);
    }
    #rw=hostname[:hostname]
    if ($temp =~ m/^rw=/) {
      #Remove rw= from the beginning of the line
      $temp =~ s/^rw=//;
      add_elements($vol, $temp, \%vol_rw_hash);
    }
    #root=hostname[:hostname]
    if ($temp =~ m/^root=/) {
      #Remove rw= from the beginning of the line
      $temp =~ s/^root=//;
      add_elements($vol, $temp, \%vol_root_hash);
    }
  #END: foreach $temp (@opt_elements)
  }
  return;
#END: sub parse_line
}




sub read_voltrans_file {
  my $infile=$_[0];
  my $srccont=$_[1];
  my $trans_hashref=$_[2];
  my ($ocont, $ovol, $ncont, $nvol) = "";
  if ($verbose) {print "	read_voltrans_file($infile, $srccont, $trans_hashref)\n"};
  if ($debug) { print "		--------------------------\n";}    
  open(VOLTRANS,$infile)||die "Failed to open: [$infile]";
  while (<VOLTRANS>) {
    my $line = $_;
    #Remove newline
    chomp ($line);		    # Remove New Line
    $line =~ s/\r//g;		    # Remove Carriage Return 
    if ($_ =~ /^\s*#/) { next; }    # Skip lines beginning with #
    if ($_ =~ /^\s*$/) { next; }    # Skip lines with only whitespace
    # Split each line on comma
    ($ocont, $ovol, $ncont, $nvol) = split (/,/,$line);
    if ($debug) { print "		Translating: $ocont:$ovol to $ncont:$nvol\n";}  
    if ($ocont eq $srccont) {
      # Translation line matches this export file's host (srccont). 
	  	# Add the entry into our translation hash
	  	add_element($ovol,$ncont.":".$nvol,$trans_hashref);    
    } else {
      # Entry doesn't match the source controller so skip it.
      if ($debug) { print "			Skipping: $ocont doesn't match $srccont.\n";}  
    }#if ($ocont eq $srccont)
  }
  close (VOLTRANS);
  if ($debug) { print "		--------------------------\n";}  
  if ($verbose) {print "	Completed read_voltrans_file\n"}
  return;
}


sub generate_cdot_script {
  my $translate  = $_[0];
  my $srccont    = $_[1];
  my $trans_ref  = $_[2];
  my $opt_ref    = $_[3];  
  my $ro_ref     = $_[4];   
  my $rw_ref     = $_[5];   
  my $root_ref   = $_[6];
  my $output_ref = $_[7]; 
  my @opts = ();
  my ($opt, $vol, $string, $host) = "";
  
  if ($verbose) { print "generate_cdot_script($translate, $srccont,$trans_ref,$opt_ref,$ro_ref,$rw_ref,$root_ref,$output_ref)\n";}
  # Iterate through all keys in the options hash (every export has at least one option)
  foreach $vol (sort (keys (% $opt_ref))) {  
    my ($clientmatch, $sec_str, $anon_str, $suid_str, $ro, $rw, $nosuid_str,$destvol,$destsvm) = ""; 
    my @hosts = ();  # Hosts associated with this vol
    
    # Debugging Output - All key/value pairs required to make rules.
    if ($debug) { 
      print "\n	-------------------------\n";
      print "	KEY: $vol\n";
      print "	-------------------------\n";
      if (exists $trans_ref->{$vol}) { print "		TARGET: $trans_ref->{$vol}\n";}  
      if (exists $opt_ref->{$vol})   { print "		OPT: $opt_ref->{$vol}\n";}            
      if (exists $ro_ref->{$vol})    { print "		RO: $ro_ref->{$vol}\n";}
      if (exists $rw_ref->{$vol})    { print "		RW: $rw_ref->{$vol}\n";}
      if (exists $root_ref->{$vol})  { print "		ROOT: $root_ref->{$vol}\n";}
      print "		-------------------------\n";
    }# if ($debug)

    # If translation is enabled, and there is not a translation, skip this volume.
    if ((!exists $trans_ref->{$vol}) && ($translate eq "true")) {
      # Skip this volume.  Show warning always!!!
      print "	WARNING: No translation entry for volume, skipping $vol. $trans_ref->{$vol}  $translate  \n";
      next;
    } else {
      if ($debug) { print "	Translating $vol. >$trans_ref->{$vol}<\n";}
      # We have a translation, so populate the variables.
      ($destsvm,$destvol) = split (/:/,$trans_ref->{$vol});   
    }
    if ($translate ne "true") {
      $destvol = $vol;;
      $destsvm = "SVM_NAME";
    }
    if ($debug) { print "	TO: >$destsvm<   >$destvol<\n";}

    # Build option strings for use below
    @opts = split (/:/,$opt_ref->{$vol});
    foreach $opt (@opts) {
      if ($opt =~ m/sec=/) {
        $sec_str = $opt;
        $sec_str =~ s/sec=//;
      } elsif ($opt =~ m/anon=/) {
        $anon_str = $opt;
        $anon_str =~ s/anon=//;
      } elsif ($opt =~ m/nosuid/) {
        $suid_str = "false";
      } elsif ($opt =~ m/^ro$/) {
        $ro = "true";       
      } elsif ($opt =~ m/^rw$/) {
        $rw = "true";
      } else { print "ERROR: Unknown option $opt for vol $vol\n"; }
    } #foreach $opt

    # Check option elements now.
    if ($debug) { print "	     check: RW>$rw< RO>$ro< SUID>$suid_str< ANON>$anon_str< SEC>$sec_str<\n";}

    # Build the list of unique hosts for this export. 
    if (exists $ro_ref->{$vol}) { add_array_elements($ro_ref->{$vol}, \@hosts); }
    if (exists $rw_ref->{$vol}) { add_array_elements($rw_ref->{$vol}, \@hosts); }
    if (exists $root_ref->{$vol}) { add_array_elements($root_ref->{$vol}, \@hosts); }
    # Debugging output - verify we've built our hosts hash correctly    
    if ($debug) { print "		Hosts (clientmatch):  @hosts\n";}	

    # Generate the export policy.
    $string = "export-policy create -vserver $destsvm -policyname ".$destvol;
    if ($debug) { print "		$string\n";}
    push(@$output_ref, $string);

    # Modify the volume to use the policy
    $string = "volume modify  -vserver $destsvm -volume $destvol -policy ".$destvol;
    if ($debug) { print "		$string\n";}
    push(@$output_ref, $string);

    # Mount the volume (because TDP mirrors don't automatically get mounted).
    $string = "volume mount  -vserver $destsvm -volume $destvol -junction-path $vol_prefix/".$destvol;
    if ($debug) { print "		$string\n";}
    push(@$output_ref, $string);

    # Iterate through this list of unique hosts and generate their export rules.
    foreach $host (@hosts) {
      $string = "export-policy rule create -vserver $destsvm -policyname $destvol -protocol nfs3 -clientmatch $host"; 
      # -rorule any|none|never|krb5|ntlm|sys 
      # if there is an ro= rule, and this host is listed or there's a rw= rule and this host is listed (or ro, rw)
      if (((exists $ro_ref->{$vol}) && ($ro_ref->{$vol} =~ m/$host/)) || ((exists $rw_ref->{$vol}) && ($rw_ref->{$vol} =~ m/$host/)) || ($ro eq "true") || ($rw eq"true")) {
        $string = $string . " -rorule " . $sec_str; 
      } #if
      # -rwrule any|none|never|krb5|ntlm|sys 
      if (((exists $rw_ref->{$vol}) && ($rw_ref->{$vol} =~ m/$host/)) || ($rw eq "true")) {
        $string = $string . " -rwrule " . $sec_str; 
      }
      # -superuser any|none|krb5|ntlm|sys
      if ((exists $root_ref->{$vol}) && ($root_ref->{$vol} =~ m/$host/)) {
        $string = $string . " -superuser " . $sec_str; 
        # Order dependancy: -superuser must follow -rwrule in the checks.
        if ($string =~ m/rwrule/) {
          # $string already has a -rwrule so we don't have to alter it for valid CDOT 8.3x syntax
        } else {
          $string = $string . " -rwrule none ";
        }
      } #if
      # -allow-suid true|false
      if ($suid_str) {
        $string = $string . " -allow-suid " . $suid_str; 
      } #if
      # -allow-dev true|false   -- NOT HANDLED
      # -anon userid
      if ($anon_str) {
        $string = $string . " -anon " . $anon_str; 
      }
      # Add the string to our script.
      push(@$output_ref, $string);
      if ($debug) { print "		$string\n";}
    }#foreach $host
    
    # Now create any generic rules for this volume
    # RW  
    if ($rw eq "true") {
      $string = "export-policy rule create -vserver $destsvm -policyname $destvol -protocol nfs3 -clientmatch 0.0.0.0/0 -rorule sys -rwrule sys"; 
      # Add the string to our script.
      push(@$output_ref, $string);    
      if ($debug) { print "		$string\n";}
    }
    # RO
    if (($ro eq "true") && (!$rw eq "true")) {
      $string = "export-policy rule create -vserver $destsvm -policyname $destvol -protocol nfs3 -clientmatch 0.0.0.0/0 -rorule sys"; 
      # Add the string to our script.
      push(@$output_ref, $string);    
      if ($debug) { print "		$string\n";}
    }
    $string = "";
    push(@$output_ref, $string);
  }# foreach $vol
  $string = "";
  push(@$output_ref, $string);
  # CDOT Script built in @$output_ref.  
  return;
}


sub output_cdot_script {
  my $outputfile = $_[0];
  my $output_ref = $_[1]; 

  open my $out, '>>', $outputfile or die "Can't open $outputfile: $!\n"; 
  foreach (@$output_ref) {    
    print $out "$_ \n";                        
  } 
  close $out;

  return;
}


sub usage {
  print "\nexports2cdot -f export_file [-o output_file] [-t translation_file -s src_controller] [-h] [-v]\n";
  print "    -f exports_file      specifies the /etc/exports file you wish to parse.\n";
  print "    -o output_file       the name you wish to use for the generated CDOT script.\n";
  print "    -m junction_path_prefix  the path under which to mount the volume, eg. In 7mode it was /vol.\n";
  print "    -t translation_file -s src_controller  specifies the name of the file with oldname,newname mappings\n";
  print "       NOTE: -t requires the use of -s to identify the source controller.\n";
  print "       TRANSLATION FILE FORMAT: src_controller,src_volume,dest_svm,dest_volume\n";
  print "       Translation file is used to rename ALL volumes to match a new CDOT naming convention.\n";
  print "       Volumes that do not exist in the translation file are skipped in parsing the export_file.  \n\n";
  print "    -v    (verbose) - shows progress.\n";
  #print "    -d   (debug) - shows troubleshooting details of each step of the parsing.\n";
  print "    -h    (help) - shows this output.\n";
  print "\n\n";
  
  return;
}

 
sub add_element {
  my $key   = $_[0];
  my $value = $_[1];
  my $hashref = $_[2];

  if ($debug) { print "		########\n			IN add_element:  $key  $value   $hashref\n";}
  if (exists $hashref->{$key}) {
    # Add value to existing value list (: delimited) if unique.
    if ( isUnique($key,$value,$hashref) ) {
      if ($debug) { print "			Append Unique Value: $key - $value\n";}
      # value is unique so we append it to the value list.
      $hashref->{$key} = join (":",$hashref->{$key},$value);
    } else { 
      if ($debug) { print "				Skipped Nonunique value: $key - $value\n"; }
    };
  } else {
    if ($debug) { print "			Insert Unique Value: $key - $value\n";}
    # New key, just set it to the value.
    $hashref->{$key} = $value;
  }   
}


sub add_array_elements {
  my $elements = $_[0];
  my $arrayref = $_[1];
  my $value = "";
  my @values = split (/:/,$elements);
  
  if ($debug) { print "			add_array_elements: @values\n";}  
  foreach $value (@values) {
    if ($value ~~ @$arrayref) {
      # Duplicate
      if ($debug) { print "				Skipping Duplicate Host: $value\n";}
    } else {
      # Unique
      if ($debug) { print "				Adding Unique Host: $value\n";}
        push (@$arrayref, $value);
    } 
  } #foreach $value
} 


sub add_elements {
  my $key   = $_[0];
  my $value = $_[1];
  my $hashref = $_[2];
  my @elements = ();
  my $field;

  # Split the : delimited value list into an array
  @elements = split (/:/,$value);
  if ($debug) { print "		********\n			IN add_elements:  $key  $value   $hashref\n			Elements: @elements\n";}
  # Iterate through the array 
  foreach $field (@elements) {
  if ($debug) { print "			Processing Element/field: $field\n";}
    # If there is an existing Key=value pair in the hash
    if (exists $hashref->{$key}) {
      # Ensure we only add the new value if its not already present
      if ( isUnique($key,$field,$hashref) ) { 
        $hashref->{$key} = join (":",$hashref->{$key},$field); 
        if ($debug) { print "				Append Unique Value: $key - $field\n";}
        } else {       
          if ($debug) { print "				Skipped Nonunique value: $key - $value\n"; }
        };
    } else {
      # Hash doesn't have an existing key=value pair so just add this one.
   if ($debug) { print "				$key  does not exist\n";} 
      $hashref->{$key} = $field;
      if ($debug) { print "				Insert Unique Key/Value: $key - $field\n";}
    }
  } 
}


sub isUnique {
  my $key     = $_[0];
  my $value   = $_[1];
  my $hashref = $_[2];
  my @elements = ();
 
  #Break : delemited hash results into an array of elements to check
  @elements = split(/:/,($hashref->{$key}));
 
  # Debugging output  
  if ($debug) { print "                                IN isUnique:  >$key< >$value<\n                                   Checking against: >@elements<\n"};
 
   # Check the contents of the array for the existence of the value
  if ( grep( /^$value$/, @elements) ) {
    return 0; # false - the value already exists
  }  else {
    return 1; # true - the value is unique
  }

  # Should never reach here
  exit(2);
}
