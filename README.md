# README #

# exports-7-to-cdot.pl
Parse the contents of a 7mode /etc/exports file and generate the commands to create a CDOT 8.x / ONTAP 9 export policy.  

Set export permissions at a volume level to the union of the volume and any qtree/subdirectory export permissions.

Note: This means you can end up giving MORE access to hosts, but you will not deny access where you used to allow it.



#Usage:   

exports-7-to-cdot.pl -f export_file [-o output_file] [-t translationfile -s srccontroller] [-h] [-v]

    -f exports_file      specifies the /etc/exports file you wish to parse.
    -o output_file       the name you wish to use for the generated CDOT script.
    
    
    -t translationfile -s srccontroller    Optional: used if you are renaming the volumes as you are migrating (ie /vol/vol1 to cdot_volume_name) 
    
       NOTE: -t requires the use of -s to identify the source controller.
       TRANSLATION FILE FORMAT: src_controller,src_volume,dest_svm,dest_volume
       Translation file is used to rename ALL volumes to match a new CDOT naming convention.
       Volumes that do not exist in the translation file are skipped in parsing the export_file.

    -v    (verbose) - shows progress.
    -h    (help) - shows this output.
